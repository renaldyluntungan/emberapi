require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const mysql = require("mysql");
var cors = require("cors");

app.use(bodyParser.json());
app.use(cors());

const pool = mysql.createPool({
  connectionLimit: 10,
  host: process.env.host,
  user: process.env.user,
  password: process.env.password,
  database: process.env.database
});

//Select all data
app.get("/employees", (req, res) => {
  pool.getConnection((err, connection) => {
    if (err) throw err;

    connection.query("SELECT * from employee", (err, rows) => {
      connection.release();
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    });
  });
});

// Select data
app.get("/employees/:id", (req, res) => {
  pool.getConnection((err, connection) => {
    if (err) throw err;
    connection.query(
      "SELECT * FROM employee WHERE id = ?",
      [req.params.id],
      (err, rows) => {
        connection.release();
        if (!err) {
          res.send(rows);
        } else {
          console.log(err);
        }
      }
    );
  });
});

// Post data
app.post("/employees", (req, res) => {
  pool.getConnection((err, connection) => {
    if (err) throw err;

    const params = req.body;
    connection.query("INSERT INTO employee SET ?", params, (err, rows) => {
      connection.release();
      if (!err) {
        res.send(`Berhasil menambahakan data`);
      } else {
        console.log(err);
      }
    });
  });
});

//Update data
app.patch("/employees/:id", (req, res) => {
  pool.getConnection((err, connection) => {
    if (err) throw err;

    const { nim, nama } = req.body;

    let id = req.params.id;
    console.log(id);
    console.log(req.body);
    connection.query(
      "UPDATE employee SET nim = ?, nama = ? WHERE id = ?",
      [nim, nama, id],
      (err, rows) => {
        connection.release();

        if (!err) {
          res.send(`Data berhasil diupdate.`);
        } else {
          console.log(err);
        }
      }
    );
  });
});

// Delete data
app.delete("/employees/:id", (req, res) => {
  pool.getConnection((err, connection) => {
    if (err) throw err;
    connection.query(
      "DELETE FROM employee WHERE id = ?",
      [req.params.id],
      (err, rows) => {
        connection.release();
        if (!err) {
          res.send(`Data berhasil di hapus.`);
        } else {
          console.log(err);
        }
      }
    );
  });
});

//port
app.listen(3000, () => {
  console.log("Server started on port 3000...");
});
